//
//  TodosGetRequest.swift
//  TodoApp
//
//  Created by hsato on 2020/07/02.
//  Copyright © 2020 hsato. All rights reserved.
//

import Alamofire

struct TodosGetRequest: RequestProtocol {
    typealias Response = TodosGetResponse

    var parameters: Parameters? {
        nil
    }
    var path: String {
        "/todos"
    }
    var method: HTTPMethod {
        .get
    }
}
