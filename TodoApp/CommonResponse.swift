//
//  CommonResponse.swift
//  TodoApp
//
//  Created by hsato on 2020/07/02.
//  Copyright © 2020 hsato. All rights reserved.
//

import Foundation

struct CommonResponse: BaseResponse {
    var errorCode: Int
    var errorMessage: String
}
