//
//  TodoEditViewController.swift
//  TodoApp
//
//  Created by hsato on 2020/06/17.
//  Copyright © 2020 hsato. All rights reserved.
//

import UIKit

protocol TodoEditViewControllerDelegate: class {
    func todoEditViewControllerDidCreateTodo(_ todoEditViewController: TodoEditViewController)
}

final class TodoEditViewController: UIViewController {
    weak var delegate: TodoEditViewControllerDelegate?

    @IBOutlet private weak var detailTextView: UITextView!
    @IBOutlet private weak var titleCountLabel: UILabel!
    @IBOutlet private weak var detailCountLabel: UILabel!
    @IBOutlet private weak var registrationButton: UIButton!
    @IBOutlet private weak var titleTextField: UITextField!
    @IBOutlet private weak var dateTextField: UITextField!

    private let dateFormatter = DateFormatter()
    private let datePicker = UIDatePicker()
    private let titleCharacterMax = 100
    private let detailCharacterMax = 1000

    private var titleText: String {
        titleTextField.text ?? ""
    }
    private var detailText: String {
        detailTextView.text ?? ""
    }
    private var isTitleCharacterOverLimit: Bool {
        titleText.count > titleCharacterMax
    }
    private var isDetailCharacterOverLimit: Bool {
        detailText.count > detailCharacterMax
    }
    private var isRegistrationButtonActive: Bool  {
        !titleText.isEmpty && !isTitleCharacterOverLimit && !isDetailCharacterOverLimit
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpRegistrationButton()
        setUpDetailTextView()
        setUpNavigationBarItem()
        setUpDatePicker()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy/M/d"
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    private func setUpNavigationBarItem() {
        navigationItem.title = "TODO APP"
    }

    private func setUpRegistrationButton() {
        registrationButton.layer.cornerRadius = 10.0
        registrationButton.setTitleColor(.white, for: .normal)
        disableRegistrationButton()
    }

    private func setUpDetailTextView() {
        detailTextView.delegate = self
        detailTextView.layer.borderWidth = 1.0
        detailTextView.layer.borderColor = UIColor.black.cgColor
    }

    private func setUpDatePicker() {
        datePicker.calendar = Calendar(identifier: .gregorian)
        datePicker.datePickerMode = .date
        datePicker.timeZone = NSTimeZone.local
        datePicker.locale = .current
        dateTextField.inputView = datePicker
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 30))
        let flexibleSpaceItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let decisionButtonItem = UIBarButtonItem(title: "決定", style: .done, target: self, action: #selector(didTapDecisionButton))
        let closeButtonItem = UIBarButtonItem(title: "閉じる", style: .plain, target: self, action: #selector(didTapCloseButton))
        let deleteButtonItem = UIBarButtonItem(title: "削除", style: .plain, target: self, action: #selector(didTapDeleteButton))

        toolbar.setItems([deleteButtonItem, flexibleSpaceItem, closeButtonItem, decisionButtonItem], animated: true)
        dateTextField.inputAccessoryView = toolbar
    }

    @objc private func didTapDecisionButton() {
        dateTextField.endEditing(true)
        dateTextField.text = dateFormatter.string(from: datePicker.date)
    }

    @objc private func didTapCloseButton() {
        dateTextField.endEditing(true)
    }

    @objc private func didTapDeleteButton() {
        dateTextField.endEditing(true)
        dateTextField.text = ""
    }

    private func enableRegistrationButton() {
        registrationButton.isEnabled = true
        registrationButton.backgroundColor = .blue
    }

    private func disableRegistrationButton() {
        registrationButton.isEnabled = false
        registrationButton.backgroundColor = .gray
    }

    private func updateActiveStateOnRegistrationButton() {
        if isRegistrationButtonActive {
            enableRegistrationButton()
        } else {
            disableRegistrationButton()
        }
    }

    private func registerTodo(title: String, detail: String?, date: Date?) {
        APIClient().call(
            request: TodoPostRequest(title: title, detail: detail, date: date),
            success: { [weak self] _ in
                guard let self = self else { return }
                self.delegate?.todoEditViewControllerDidCreateTodo(self)
            },
            failure: { [weak self] message in
                self?.showErrorAlert(message: message)
            }
        )
    }

    @IBAction private func didChangeTextField(_ sender: Any) {
        titleCountLabel.text = String(titleText.count)
        titleCountLabel.textColor = isTitleCharacterOverLimit ? .red : .black
        updateActiveStateOnRegistrationButton()
    }
    
    @IBAction private func onClickRegistrationButton(_ sender: Any) {
        guard let title = titleTextField.text else { return }
        let detail = detailTextView.text.isEmpty ? nil : detailTextView.text
        let dateText = dateTextField.text ?? ""
        let date = dateFormatter.date(from: dateText)
        registerTodo(title: title, detail: detail, date: date)
    }
}

extension TodoEditViewController: UITextViewDelegate {

    func textViewDidChange(_ textView: UITextView) {
        detailCountLabel.text = String(detailText.count)
        detailCountLabel.textColor = isDetailCharacterOverLimit ? .red : .black
        updateActiveStateOnRegistrationButton()
    }
}
