//
//  UIViewController+.swift
//  TodoApp
//
//  Created by hsato on 2020/07/10.
//  Copyright © 2020 hsato. All rights reserved.
//

import UIKit

extension UIViewController {
    func showErrorAlert(message: String) {
        let alertController = UIAlertController(title: "エラー", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default))
        present(alertController, animated: true)
    }
}
