//
//  TodoListViewController.swift
//  TodoApp
//
//  Created by hsato on 2020/06/11.
//  Copyright © 2020 hsato. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialSnackbar

final class TodoListViewController: UIViewController {
    @IBOutlet private weak var todoTableView: UITableView!
    private var composeBarButtonItem: UIBarButtonItem!
    private var isDeleteMode = false
    private var todos: [Todo] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        todoTableView.dataSource = self
        setUpNavigationItem()
        fetchTodoList()
    }

    private func fetchTodoList() {
        APIClient().call(
            request: TodosGetRequest(),
            success: { [weak self] result in
                // 取得成功時の処理
                self?.todos = result.todos
                self?.todoTableView.reloadData()
            },
            failure: { [weak self] errorMessage in
                // 取得失敗時の処理
                self?.showErrorAlert(message: errorMessage)
            }
        )
    }

    private func setUpNavigationItem() {
        navigationItem.title = "TODO APP"
        let trashBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(onTappedTrashBarButton(_:)))
        composeBarButtonItem = UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(onTappedComposeBarButton(_:)))
        navigationItem.rightBarButtonItems = [trashBarButtonItem, composeBarButtonItem]
        navigationItem.backBarButtonItem = UIBarButtonItem()
    }

    @objc private func onTappedComposeBarButton(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Edit", bundle: nil)
        guard let todoEditViewController = storyboard.instantiateViewController(withIdentifier: "TodoEditView") as? TodoEditViewController else { return }
        todoEditViewController.delegate = self
        navigationController?.pushViewController(todoEditViewController, animated: true)
    }

    @objc private func onTappedTrashBarButton(_ sender: UIBarButtonItem) {
        isDeleteMode.toggle()
        let systemItem: UIBarButtonItem.SystemItem = isDeleteMode ? .stop : .trash
        let trashBarButtonItem = UIBarButtonItem(barButtonSystemItem: systemItem, target: self, action: #selector(onTappedTrashBarButton(_:)))
        navigationItem.rightBarButtonItems = [trashBarButtonItem, composeBarButtonItem]
    }

    private func showSnackbar(text: String) {
        let message = MDCSnackbarMessage()
        message.text = text
        MDCSnackbarManager.show(message)
    }
}

extension TodoListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        todos.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodoCell", for: indexPath)
        cell.textLabel?.text = todos[indexPath.row].title
        return cell
    }
}

extension TodoListViewController: TodoEditViewControllerDelegate {
    func todoEditViewControllerDidCreateTodo(_ todoEditViewController: TodoEditViewController) {
        navigationController?.popViewController(animated: true)
        showSnackbar(text: "登録しました")
        fetchTodoList()
    }
}
