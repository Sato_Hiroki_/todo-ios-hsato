//
//  Todo.swift
//  TodoApp
//
//  Created by hsato on 2020/07/02.
//  Copyright © 2020 hsato. All rights reserved.
//

import Foundation

struct Todo: Codable {
    let id: Int
    let title: String
    let detail: String?
    let date: Date?
}
