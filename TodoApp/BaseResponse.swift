//
//  BaseResponse.swift
//  TodoApp
//
//  Created by hsato on 2020/07/02.
//  Copyright © 2020 hsato. All rights reserved.
//

import Foundation

protocol BaseResponse: Codable {
    var errorCode: Int { get }
    var errorMessage: String { get }
}
